package ua.ithillel.hillelwebapp.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "orders")
public class Order {

    @Id
    private int id;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Europe/Kiev")
    private Date date;

    @OneToOne
    @JsonIgnoreProperties(value = {"manufacturer", "model", "year", "price", "available"})
    @JoinColumn(name = "car_id")
    private Car car;

    @OneToOne
    @JsonIgnoreProperties(value = {"name", "surname", "phone"})
    @JoinColumn(name = "client_id")
    private Client client;

    @OneToOne
    @JsonIgnoreProperties(value = {"name", "login", "password", "email", "role"})
    @JoinColumn(name = "manager_id")
    private Manager manager;

}
