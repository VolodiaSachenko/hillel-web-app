package ua.ithillel.hillelwebapp.entity;

public enum Role {
    ADMIN,
    MANAGER,
    CLIENT
}

