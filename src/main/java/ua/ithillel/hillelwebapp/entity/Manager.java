package ua.ithillel.hillelwebapp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "managers")

public class Manager {
    @Id
    private int id;
    private String name;
    private String login;
    private String password;
    private String email;

    @Enumerated
    private Role role;
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "manager_id")
    private List<Order> orders;

}
