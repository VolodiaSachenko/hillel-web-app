package ua.ithillel.hillelwebapp.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "clients")
public class Client {
    @Id
    private int id;
    private String name;
    private String surname;
    private String phone;
}
