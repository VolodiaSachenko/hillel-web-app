package ua.ithillel.hillelwebapp.servlet;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ua.ithillel.hillelwebapp.entity.Client;
import ua.ithillel.hillelwebapp.service.ClientService;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "ClientServlet", urlPatterns = "/clients")
public class ClientServlet extends HttpServlet {

    private final ClientService clientService = new ClientService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Client> allClients = clientService.getAllClients();
        req.setAttribute("clientsList", allClients);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("clients.jsp");
        requestDispatcher.forward(req, resp);
    }
}