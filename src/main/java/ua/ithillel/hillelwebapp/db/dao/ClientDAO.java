package ua.ithillel.hillelwebapp.db.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ua.ithillel.hillelwebapp.db.session.HibernateSessionFactory;
import ua.ithillel.hillelwebapp.entity.Client;

import java.util.List;

public class ClientDAO implements AbstractDAO<Client> {

    @Override
    public Client findById(int id) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Client client = session.get(Client.class, id);
        session.close();
        return client;
    }

    @Override
    public void save(Client entity) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.persist(entity);
        tx1.commit();
        session.close();
    }

    @Override
    public Client update(Client entity) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        Client updated = session.merge(entity);
        tx1.commit();
        session.close();
        return updated;
    }

    @Override
    public void delete(Client entity) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.remove(entity);
        tx1.commit();
        session.close();
    }

    @Override
    public List<Client> findAll() {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        List<Client> clients = session.createQuery("From Client order by id asc ", Client.class).getResultList();
        session.close();
        return clients;
    }
}
