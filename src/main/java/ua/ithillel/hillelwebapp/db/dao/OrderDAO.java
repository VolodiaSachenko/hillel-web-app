package ua.ithillel.hillelwebapp.db.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ua.ithillel.hillelwebapp.db.session.HibernateSessionFactory;
import ua.ithillel.hillelwebapp.entity.Order;

import java.util.List;

public class OrderDAO implements AbstractDAO<Order> {
    @Override
    public Order findById(int id) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Order order = session.get(Order.class, id);
        session.close();
        return order;
    }

    @Override
    public void save(Order entity) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(entity);
        transaction.commit();
        session.close();
    }

    @Override
    public Order update(Order entity) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Order order = session.merge(entity);
        transaction.commit();
        session.close();
        return order;
    }

    @Override
    public void delete(Order entity) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(entity);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Order> findAll() {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        List<Order> orders = session.createQuery("From Order order by id asc ", Order.class).getResultList();
        session.close();
        return orders;
    }
}
