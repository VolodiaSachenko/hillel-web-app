package ua.ithillel.hillelwebapp.service;

import ua.ithillel.hillelwebapp.db.dao.ClientDAO;
import ua.ithillel.hillelwebapp.entity.Client;

import java.util.List;

public class ClientService {

    private final ClientDAO clientDAO = new ClientDAO();

    public List<Client> getAllClients() {
        return clientDAO.findAll();
    }

    public void save(Client client) {
        clientDAO.save(client);
    }

    public Client findById(int clientId) {
        return clientDAO.findById(clientId);
    }

    public void delete(Client clientToDelete) {
        clientDAO.delete(clientToDelete);
    }

    public Client update(Client client) {
        return clientDAO.update(client);
    }
}
