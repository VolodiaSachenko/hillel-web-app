package ua.ithillel.hillelwebapp.service;

import ua.ithillel.hillelwebapp.db.dao.CarDAO;
import ua.ithillel.hillelwebapp.entity.Car;

import java.util.List;

public class CarService {

    private final CarDAO carDAO = new CarDAO();

    public List<Car> getAllCars() {
        return carDAO.findAll();
    }

    public void save(Car car) {
        carDAO.save(car);
    }

    public Car findById(int carId) {
        return carDAO.findById(carId);
    }

    public void delete(Car carToDelete) {
        carDAO.delete(carToDelete);
    }

    public Car update(Car car) {
        return carDAO.update(car);
    }
}
