package ua.ithillel.hillelwebapp.service;

import ua.ithillel.hillelwebapp.db.dao.OrderDAO;
import ua.ithillel.hillelwebapp.entity.Order;

import java.util.List;

public class OrderService {

    private final OrderDAO orderDAO = new OrderDAO();

    public List<Order> getAllOrders() {
        return orderDAO.findAll();
    }

    public void save(Order order) {
        orderDAO.save(order);
    }

    public Order findById(int orderId) {
        return orderDAO.findById(orderId);
    }

    public void delete(Order orderToDelete) {
        orderDAO.delete(orderToDelete);
    }

    public Order update(Order order) {
        return orderDAO.update(order);
    }
}
