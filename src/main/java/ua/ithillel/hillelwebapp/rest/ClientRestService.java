package ua.ithillel.hillelwebapp.rest;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import lombok.NoArgsConstructor;
import ua.ithillel.hillelwebapp.entity.Client;
import ua.ithillel.hillelwebapp.service.ClientService;

import java.util.List;

@Path("/clients")
@NoArgsConstructor
public class ClientRestService {

    ClientService clientService = new ClientService();

    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Client> getClients() {
        return clientService.getAllClients();
    }

    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addClient(Client client) {
        clientService.save(client);
    }

    @PUT
    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Client updateClient(Client client) {
        return clientService.update(client);
    }

    @DELETE
    @Path("/delete/{id}")
    public void deleteClient(@PathParam("id") int clientId) {
        Client clientToDelete = clientService.findById(clientId);
        clientService.delete(clientToDelete);
    }
}
