package ua.ithillel.hillelwebapp.rest;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import lombok.NoArgsConstructor;
import ua.ithillel.hillelwebapp.entity.Car;
import ua.ithillel.hillelwebapp.service.CarService;

import java.util.List;

@Path("/cars")
@NoArgsConstructor
public class CarRestService {

    CarService carService = new CarService();

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Car> getCars() {
        return carService.getAllCars();
    }

    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addCar(Car car) {
        carService.save(car);
    }

    @PUT
    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Car modifyCar(Car car) {
        return carService.update(car);
    }

    @DELETE
    @Path("/delete/{id}")
    public void deleteCar(@PathParam("id") int carId) {
        Car carToDelete = carService.findById(carId);
        carService.delete(carToDelete);
    }
}

