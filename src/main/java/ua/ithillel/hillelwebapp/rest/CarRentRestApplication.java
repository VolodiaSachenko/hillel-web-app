package ua.ithillel.hillelwebapp.rest;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

@ApplicationPath("rest")
public class CarRentRestApplication extends Application {

    public CarRentRestApplication() {

    }

}
