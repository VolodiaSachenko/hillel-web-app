package ua.ithillel.hillelwebapp.rest;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import lombok.NoArgsConstructor;
import ua.ithillel.hillelwebapp.entity.Order;
import ua.ithillel.hillelwebapp.service.OrderService;

import java.util.List;

@Path("/orders")
@NoArgsConstructor
public class OrderRestService {
    OrderService orderService = new OrderService();

    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Order> getOrders() {
        return orderService.getAllOrders();
    }

    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addOrder(Order order) {
        orderService.save(order);
    }

    @PUT
    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Order updateOrder(Order order) {
        return orderService.update(order);
    }

    @DELETE
    @Path("/delete/{id}")
    public void deleteOrder(@PathParam("id") int orderId) {
        Order orderToDelete = orderService.findById(orderId);
        orderService.delete(orderToDelete);
    }
}
